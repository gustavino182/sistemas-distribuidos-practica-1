package client;

import java.net.*;
import java.io.*;

import rmi.*;

public class EchoObjectStub implements EchoInt {

  private Socket echoSocket = null;
  private PrintWriter os = null;
  private BufferedReader is = null;
  private String host = "172.30.107.245";
  private int port=4000;
  private String output = "Error";

  public EchoObjectStub() {}
  
  public EchoObjectStub(String host, int port) {
    this.host= host; this.port =port;
  }

  public String echo(String input)
  {
    connect();
    if (echoSocket != null && os != null && is != null) {
  	try {
             os.println(input);
             os.flush();
             output= is.readLine();
      } catch (IOException e) {
        System.err.println("I/O failed in reading/writing socket");
      }
    }
    disconnect();
    return output;
  }

  private synchronized void connect()
  {
	//Implementar el método connect  
    try {
        echoSocket = new Socket(host, port);
        os = new PrintWriter(echoSocket.getOutputStream());
        is = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
    } catch (UnknownHostException e) {
        System.err.println("Don't know about host: " + host);
    } catch (IOException e) {
        System.err.println("Couldn't get I/O for the connection to: " + host);
    }
  }

  private synchronized void disconnect(){ 
	//EJERCICIO: Implemente el m�todo disconnect
    try {
        os.close();
        is.close();
        echoSocket.close();
    } catch (IOException e) {
        System.err.println("Couldn't get I/O for the connection to: " + host);
    }
	  
  }
}
